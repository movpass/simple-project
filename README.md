# Simple Project

This project represents a simple example of Movpass' backend stack. 

**Requirements:**
- Docker & Docker Compose
- Python3 (virtualenv is advised)

**Running Project**
> docker-compose up -d postgres (to start postgres as daemon)

> docker-compose up movpass (to start flask app)


_If first time launching stack, you'll need to create the database. From another terminal, execute:_
> docker-compose exec movpass flask db upgrade

**Accessing**
From browser, access to view the Swagger:

> http://localhost:5000/api/swagger 

**Stack**
- PostgreSQL
- Flask (flask-restx)
- Migrate
- SQLAlchemy
- Marshmallow
