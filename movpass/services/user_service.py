import logging
from movpass.config import db
from movpass.services.models import User

log = logging.getLogger(__name__)


class UserService:
    @staticmethod
    def get_user(user_id):
        log.debug("Getting User %s", user_id)
        client = User.query.get(user_id)
        log.debug("Found %s", client)

        return client

    @staticmethod
    def list_users():
        log.debug("Listing Users")
        users = User.query.all()
        log.debug("Found %s Users: ", len(users))

        return users

    @staticmethod
    def get_user_by_email(email: str) -> User:
        log.debug("Getting User by Email: %s", email)
        user = User.query.filter_by(email=email).first()
        log.debug("Found %s", user)
        return user

    @staticmethod
    def save_user(user: User) -> User:
        log.debug("Saving/Updating user: %s", user)
        db.session.add(user)
        db.session.commit()
        log.debug("User Saved/Updated %s", user)
        return user
