import enum
from movpass.config import db


class UserGender(enum.Enum):
    male = "male"
    female = "female"
    other = "other"


class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    email = db.Column(db.String(256), nullable=False)
    phone = db.Column(db.String(256), nullable=True)
    gender = db.Column(db.Enum(UserGender), nullable=True)
    birthdate = db.Column(db.Date, nullable=True)
