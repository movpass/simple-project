import logging
from movpass.config import app, api
from movpass.apiresources.users.users_resource import UsersResource
from werkzeug.middleware.proxy_fix import ProxyFix

logging.basicConfig(level=logging.ERROR)
log = logging.getLogger(__name__)


client_ns = api.namespace("users", description="Users Functions")
client_ns.add_resource(UsersResource, "/")


def create_app():
    app.wsgi_app = ProxyFix(app.wsgi_app)
    return app.wsgi_app


if __name__ == "__main__":
    app.run()
