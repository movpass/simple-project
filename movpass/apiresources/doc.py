from flask_restx import fields

from movpass.config import api


user = api.model(
    "user_data",
    {
        "name": fields.String(description="User Name", required=True),
        "email": fields.String(description="User Email", required=True),
        "phone": fields.String(description="User Phone", required=True),
        "gender": fields.String(description="User Gender [Options: male, female, other]", required=False),
        "birthdate": fields.Date(description="User birthdate (YYYY-MM-DD)", required=False),
    },
)
