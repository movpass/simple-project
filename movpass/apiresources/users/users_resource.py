import logging
from datetime import datetime
from flask import abort
from flask_restx import Resource
from movpass.config import api
from movpass.services.user_service import UserService
from movpass.services.models import User, UserGender
from movpass.apiresources.doc import user
from movpass.apiresources.schemas import UserSchema

log = logging.getLogger(__name__)

users_schema = UserSchema(many=True)
user_schema = UserSchema(many=False)


class UsersResource(Resource):
    @api.doc(responses={200: "Success"})
    def get(self):
        log.debug("Listing users")
        users = User.query.all()
        log.debug("Found %s users", len(users))
        return users_schema.dump(users)

    @api.doc(responses={201: "Success"})
    @api.expect(user)
    def post(self):
        try:
            result = UserService.save_user(UsersResource._extract_user_from_payload(api.payload))
            log.info("Saved: %s", result)
            return user_schema.dump(result), 201
        except Exception:
            log.exception("Failed to Save/Update User")
            abort(500, "Failed to Save/Update User")

    @staticmethod
    def _extract_user_from_payload(payload: dict) -> User:
        user = User()
        user.name = payload["name"]
        user.email = payload["email"]
        user.phone = payload["phone"]
        user.gender = UsersResource._extract_gender_from_payload(payload)

        birthdate = payload.get("birthdate", None)
        if birthdate:
            birthdate = datetime.strptime(birthdate, "%Y-%m-%d").date()
        user.birthdate = birthdate
        user.height = payload.get("height", None)
        return user

    @staticmethod
    def _extract_gender_from_payload(payload) -> UserGender:
        gender_str = payload.get("gender", None)
        gender = None
        if gender_str:
            try:
                gender = UserGender[gender_str]
            except KeyError:
                log.exception("Failed to extract Gender: %s", gender_str)

        return gender
