from movpass.services.models import *
from movpass.config import app, ma
from marshmallow_enum import EnumField


class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User
        load_instance = True

    gender = EnumField(UserGender, by_value=True)
