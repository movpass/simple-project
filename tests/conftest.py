import pytest
import pkg_resources
from movpass.server import app
from movpass.config import db


@pytest.fixture
def app_client_test():
    app.config["TESTING"] = True
    app.testing = True

    # This creates an in-memory sqlite db
    # See https://martin-thoma.com/sql-connection-strings/
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    app_client_test = app.test_client()
    with app.app_context():
        db.Model.metadata.create_all(bind=db.engine)
        db.session.commit()

    yield app_client_test
    db.drop_all()
